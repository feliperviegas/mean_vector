import spacy
from collections import Counter
# To install language --> python -m spacy download en_core_web_sm
NLP = spacy.load("en_core_web_sm")


class ParseRaw:
    def __init__(self, split_file, document_file, label_file, fold, save_path):
        self.split_file = split_file
        self.document_file = document_file
        self.label_file = label_file
        self.fold = fold
        self.save_path = save_path

    @staticmethod
    def tokenization(text):
        doc = NLP(text)
        tokens = []
        for token in doc:
            if not token.is_stop and not token.is_punct:
                tokens.append(token.text)

        return tokens

    def load_splits_ids(self, with_val=False):
        splits = []
        with open(self.split_file, encoding='utf8', errors='ignore') as fileout:
            for line in fileout.readlines():
                parts = line.split(';')
                if len(parts) == 2:
                    train_index, test_index = parts
                    train_index = list(map(int, train_index.split()))
                    test_index = list(map(int, test_index.split()))
                    splits.append((train_index, test_index))
                elif len(parts) == 3:
                    train_index, val_index, test_index = parts
                    test_index = list(map(int, test_index.split()))
                    val_index = list(map(int, val_index.split()))
                    train_index = list(map(int, train_index.split()))
                    if not with_val:
                        train_index.extend(val_index)
                        val_index = []
                    splits.append((train_index, val_index, test_index))
                else:
                    raise Exception("")
        return splits

    def read_file_save_array(self):
        X_raw = []
        with open(self.document_file, 'r') as file:
            for document in file:
                doc_arrar = []
                tokens = self.tokenization(document.strip())
                for iter_token in range(0, len(tokens)):
                    doc_arrar.append(tokens[iter_token])

                X_raw.append(doc_arrar)

            file.close()

        return X_raw

    def read_labels(self):
        y = []
        with open(self.label_file, 'r') as file:
            for document in file:
                y.append(document.strip())

            file.close()

        return y

    @staticmethod
    def get_array(data, idxs):
        return [data[idx] for idx in idxs]

    def save_file(self, X, y, filename_type="train"):
        with open("{path}/d_{type}_data_{fold}.txt".format(type=filename_type,
                                                           path=self.save_path,
                                                           fold=self.fold), 'w') as out:
            for doc in X:
                if len(doc) > 0:
                    out.write('{}'.format(doc[0]))
                    for word_id in range(1, len(doc)):
                        out.write(' {}'.format(doc[word_id]))

                    out.write('\n')
                else:
                    out.write('\n')

            out.close()

        with open("{path}/c_{type}_data_{fold}.txt".format(type=filename_type,
                                                           path=self.save_path,
                                                           fold=self.fold), 'w') as out:
            for doc in y:
                out.write('{}\n'.format(doc.strip()))

            out.close()

    def run(self):
        splits_documents = self.load_splits_ids()
        X_raw = self.read_file_save_array()
        y = self.read_labels()
        # train_idx, val_idx, test_idx = splits_documents[self.fold]
        train_idx, test_idx = splits_documents[self.fold]
        X_train = self.get_array(X_raw, train_idx)
        y_train = self.get_array(y, train_idx)
        X_test = self.get_array(X_raw, test_idx)
        y_test = self.get_array(y, test_idx)
        self.save_file(X=X_train, y=y_train)
        self.save_file(X=X_test, y=y_test, filename_type="test")

    def check_class_distribution(self):
        splits_documents = self.load_splits_ids()
        y = self.read_labels()
        # train_idx, val_idx, test_idx = splits_documents[self.fold]
        train_idx, test_idx = splits_documents[self.fold]
        y_train = self.get_array(y, train_idx)
        y_test = self.get_array(y, test_idx)
        print('Training Set:')
        print(f'{Counter(y_train)}')
        print('Test Set:')
        print(f'{Counter(y_test)}')

