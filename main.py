import os
import argparse
from parse_splits import ParseRaw
from script_functions import create_embedding_models
from script_functions import generate_representation


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset',
                        action='store',
                        type=str,
                        dest='dataset',
                        help='-d [dataset folder name]')
    parser.add_argument('-t', '--texts',
                        action='store',
                        type=str,
                        dest='texts',
                        help='-t [texts folder name]')
    parser.add_argument('-l', '--labels',
                        action='store',
                        type=str,
                        dest='labels',
                        help='-l [labels folder name]')
    parser.add_argument('-s', '--split',
                        action='store',
                        type=str,
                        dest='split',
                        help='-s [splits folder name]')
    parser.add_argument('-f', '--fold',
                        action='store',
                        type=int,
                        dest='fold',
                        help='--fold [TRAIN/TEST FOLD]')
    # parser.add_argument('-c', '--n_classes',
    #                     action='store',
    #                     type=int,
    #                     dest='n_classes',
    #                     help='--n_classes [Number of classes]')
    args = parser.parse_args()
    # Paths and files paths
    MAIN_PATH = '/mean_vector_repr'
    EMBEDDING_RESULTS = 'fasttext_wiki'
    PATH_TO_SAVE_RESULTS = '{}/{}/results'.format(MAIN_PATH, EMBEDDING_RESULTS)
    EMBEDDINGS_FILE_PATH = '/{}/wiki-news-300d-1M.vec'.format(MAIN_PATH)
    EMBEDDINGS_BIN_TYPE = False
    DATASET = args.dataset
    PATH_TO_SAVE_REPRESENTATION = '{}/{}/datasets/{dataset}'.format(MAIN_PATH, EMBEDDING_RESULTS,
                                                                    dataset=DATASET)

    print('{}/{}/datasets/{dataset}'.format(MAIN_PATH, EMBEDDING_RESULTS, dataset=args.dataset))
    try:
        os.mkdir('{}/{}'.format(MAIN_PATH, EMBEDDING_RESULTS))
    except FileExistsError:
        pass

    try:
        os.mkdir('{}/{}/datasets'.format(MAIN_PATH, EMBEDDING_RESULTS))
    except FileExistsError:
        pass

    try:
        os.mkdir('{path}'.format(path=PATH_TO_SAVE_REPRESENTATION))
    except FileExistsError:
        pass

    ParseRaw(split_file=args.split,
             document_file=args.texts,
             label_file=args.labels,
             fold=args.fold,
             save_path=PATH_TO_SAVE_REPRESENTATION).run()

    #Create the word2vec models for each dataset
    print('Filter embedding space to {} dataset...'.format(DATASET))

    n_words = create_embedding_models(dataset=DATASET,
                                      embedding_file_path=None,
                                      embedding_type=EMBEDDINGS_BIN_TYPE,
                                      datasets_path='{path}/d_train_data_{fold}.txt'.format(
                                                        path=PATH_TO_SAVE_REPRESENTATION,
                                                        fold=args.fold),
                                      path_to_save_model=PATH_TO_SAVE_REPRESENTATION,
                                      fold=args.fold)

    print('Build representations...')
    generate_representation(training_features='{path}/d_train_data_{fold}.txt'.format(
                                path=PATH_TO_SAVE_REPRESENTATION,
                                fold=args.fold),
                            training_classes='{path}/c_train_data_{fold}.txt'.format(
                                path=PATH_TO_SAVE_REPRESENTATION,
                                fold=args.fold),
                            n_words=n_words,
                            test_features='{path}/d_test_data_{fold}.txt'.format(path=PATH_TO_SAVE_REPRESENTATION,
                                                                                 fold=args.fold),
                            test_classes='{path}/c_test_data_{fold}.txt'.format(path=PATH_TO_SAVE_REPRESENTATION,
                                                                                fold=args.fold),
                            path_to_save_model=PATH_TO_SAVE_REPRESENTATION,
                            fold=args.fold,
                            dataset_name=args.dataset,
                            path_to_save_results=PATH_TO_SAVE_RESULTS)


if __name__ == '__main__':
    main()
