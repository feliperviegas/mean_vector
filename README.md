# Mean Vector Representation

Python3 requirements:
- numpy
- scipy
- gensim
- pandas
- scikit-learn
- joblib
- matplotlib
- prettytable
- nimfa
- pillow
- seaborn

Word Embedding exploited:
- [FastText](https://fasttext.cc/docs/en/english-vectors.html) (Pre-trained Word Embedding)

Once installed, setup the paths in the startup file :

```main.py```

Build docker container:

```docker build -t mean_vector_repr .```

Run docker container:

```docker run --rm --name mean_vector_repr --env-file=env -v <project_path>:/mean_vector_repr -i -t mean_vector_repr /bin/bash```

To run the code:

```python3 main.py -d <dataset> -t <texts> -l <labels> -s <split sampling> -f <fold id>```

After running docker container, run the following code

```EXPORT LANG="C.UTF-8"```

For more information about building and running a docker container, see: https://docs.docker.com/


