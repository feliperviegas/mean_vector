import os
import codecs
import numpy as np
from timeit import default_timer as timer
from embedding import CreateEmbeddingModels


def create_embedding_models(dataset, embedding_type, datasets_path, path_to_save_model, fold, embedding_file_path=None):
    # Create the word2vec models for each dataset
    word2vec_models = CreateEmbeddingModels(embedding_file_path=embedding_file_path,
                                            embedding_type=embedding_type,
                                            document_path=datasets_path,
                                            path_to_save_model=path_to_save_model)
    n_words = word2vec_models.filter_embedding_models(dataset, fold)

    return n_words


def build_word_vector_matrix(vector_file, n_words):
    numpy_arrays = []
    labels_array = []

    with codecs.open(vector_file, 'r', 'utf-8') as f:
        _ = next(f)  # Skip the first line

        for c, r in enumerate(f):
            sr = r.split()
            labels_array.append(sr[0])
            numpy_arrays.append(np.array([float(i) for i in sr[1:]]))

            if c == n_words:
                return np.array(numpy_arrays), labels_array

    return np.array(numpy_arrays), np.array(labels_array)


def transform_datasets(documents_file, word_vectors, words):
    mean_vector = []
    with open(documents_file, 'r') as documents:
        for document in documents:
            document_array = []
            doc_words = document.strip().split(' ')
            for word in doc_words:
                document_array.append(word_vectors[np.where(words == word)][0]) if word in words else next

            if len(document_array) > 0:
                document_array = np.array(document_array)
                mean_vector.append(np.mean(document_array, axis=0))
            else:
                mean_vector.append([])

        documents.close()

    return np.array(mean_vector)


def read_labels(labels):
    y = []
    with open(labels, 'r') as input_file:
        for doc_id in input_file:
            y.append(int(doc_id.strip()))

        input_file.close()
    return y


def save_representation(output_file, vectors, labels):
    with open(output_file, 'w') as output_file:
        for doc_id in range(0, vectors.shape[0]):
            output_file.write('{}'.format(labels[doc_id]))

            for dimension in range(0, vectors.shape[1]):
                output_file.write(' {}:{}'.format(dimension+1, vectors[doc_id][dimension]))

            output_file.write('\n')

        output_file.close()


def generate_representation(training_features, training_classes,
                            test_features, test_classes, n_words, path_to_save_model,
                            path_to_save_results, fold, dataset_name):

    # Path to files and directories
    embedding_file_path = """{path}/{dataset}_embedding_{fold}.txt""".format(path=path_to_save_model,
                                                                             dataset=dataset_name,
                                                                             fold=fold)

    try:
        os.mkdir('{}'.format(path_to_save_results))
    except FileExistsError:
        pass

    try:
        os.mkdir('{}/{}'.format(path_to_save_results, dataset_name))
    except FileExistsError:
        pass

    path_to_save_results = '{}/{}'.format(path_to_save_results, dataset_name)
    start = timer()

    y_train = read_labels(training_classes)
    y_test = read_labels(test_classes)
    word_vectors, words = build_word_vector_matrix(embedding_file_path, n_words)
    training_word_vector = transform_datasets(training_features, word_vectors, words)
    test_word_vector = transform_datasets(test_features, word_vectors, words)
    save_representation(output_file='{}/mean_vector_train_{}'.format(path_to_save_results, fold),
                        vectors=training_word_vector,
                        labels=y_train)
    save_representation(output_file='{}/mean_vector_test_{}'.format(path_to_save_results, fold),
                        vectors=test_word_vector,
                        labels=y_test)

    end = timer()
    print(f'Time CluWords: {end-start}')
